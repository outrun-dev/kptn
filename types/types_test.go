package types

import (
	"errors"
	"fmt"
	"strings"
	"testing"
)

func TestUnmarshalPermission(t *testing.T) {
	tests := []struct {
		input        []byte
		expectedPerm Permission
		expectedErr  error
	}{
		{
			// The buffer passed into UnmarshalJSON is always in quotes
			// because it's a string.
			[]byte(`"10100000"`),
			Permission(0xA0),
			nil,
		},
		{
			// The buffer passed into UnmarshalJSON is always in quotes
			// because it's a string.
			[]byte(`"0"`),
			Permission(0x0),
			nil,
		},
		{
			[]byte(`0`),
			Permission(0x0),
			nil,
		},
		{
			[]byte(`10100000`),
			Permission(0x0),
			errors.New("input to UnmarshalJSON not a valid JSON string"),
		},
		{
			nil,
			Permission(0x0),
			nil,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s", test.input), func(t *testing.T) {
			var perm Permission

			err := perm.UnmarshalJSON(test.input)
			if (err == nil && test.expectedErr != nil) ||
				(err != nil && test.expectedErr == nil) ||
				(test.expectedErr != nil && err != nil &&
					strings.Compare(test.expectedErr.Error(), err.Error()) != 0) {

				t.Fatalf("\nexpected err %+v\n\ngot %+v", test.expectedErr, err)
			}

			if perm != test.expectedPerm {
				t.Fatalf("\nexpected perm %08b\n\ngot %08b", test.expectedPerm, perm)
			}
		})
	}
}
