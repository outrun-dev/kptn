// Copyright © 2019 Hugo Torres hugo.torres1993@gmail.com
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

type gitPoller struct {
	logger *logrus.Entry

	url      string
	ref      string
	lastHead string

	queue chan<- []byte

	cb func(url, ref, lasthead string) error
}

func (gp *gitPoller) Poll(ctx context.Context) error {
	logger := gp.logger.WithFields(logrus.Fields{
		"poll": "git",
		"url":  gp.url,
		"ref":  gp.ref,
	})

	done := ctx.Done()
	for {
		select {
		case <-done:
			logger.Info("context done, shutting down poller")

			return nil
		default:
			logger.Info("running poller")

			err := gp.checkRepo()
			if err != nil {
				logger.WithError(err).Error("unable to clone git repo")
			}

			logger.Debug("sleeping")
			time.Sleep(1 * time.Minute)
		}
	}
}

func (gp *gitPoller) checkRepo() error {
	logger := gp.logger.WithFields(logrus.Fields{
		"poll": "git",
		"url":  gp.url,
		"ref":  gp.ref,
	})

	clonedir := fmt.Sprintf("/tmp/git-poller.%v", uuid.New())

	opts := &git.CloneOptions{
		URL:           gp.url,
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%v", gp.ref)),
		SingleBranch:  true,
		Depth:         1,
	}

	logger.Infof("cloning into %v", clonedir)

	repo, err := git.PlainClone(clonedir, false, opts)
	if err != nil {
		logger.WithError(err).Debug("unable to clone repo")
		return err
	}

	head, err := repo.Head()
	if err != nil {
		logger.WithError(err).Debug("unable to get repo HEAD")

		cleanerr := os.RemoveAll(clonedir)
		if err != nil {
			logger.WithError(cleanerr).Debugf("unable to clean up clonedir %v", clonedir)
		}

		return err
	}

	logger.Infof("got repo head %v", head)
	if head.String() != gp.lastHead {
		logger.Info("head changed")

		gp.lastHead = head.String()

		go func() {
			err := gp.cb(gp.url, gp.ref, gp.lastHead)
			if err != nil {
				logger.WithError(err).Error("callback returned error")
			}
		}()
	}

	err = os.RemoveAll(clonedir)
	if err != nil {
		logger.WithError(err).Debugf("unable to clean up clonedir %v", clonedir)
		return err
	}

	logger.Debug("clonedir successfully deleted")
	return nil
}
