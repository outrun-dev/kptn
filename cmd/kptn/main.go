// Copyright © 2019 Hugo Torres hugo.torres1993@gmail.com
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/outrun-dev/kptn/async"
	"gitlab.com/outrun-dev/kptn/ci"
	"gitlab.com/outrun-dev/kptn/db"
	"gitlab.com/outrun-dev/kptn/http"
	"gitlab.com/outrun-dev/kptn/types"
)

func main() {
	logger := getLogger()
	rootCmd := getRootCmd(logger)

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func getRootCmd(logger *logrus.Entry) *cobra.Command {
	return &cobra.Command{
		Use:   "kptn",
		Short: "ktpn -- high level CI orchestration",
		Long: `ktpn is a CI orchestrator.
	
It handles data, authentication/authorization, and detecting
changes to whatever repository you point your project to.`,
		// Uncomment the following line if your bare application
		// has an action associated with it:
		Run: func(cmd *cobra.Command, args []string) {
			logger.Info("booting kptn server")

			connstr, err := getDBConnStr()
			if err != nil {
				logger.WithError(err).
					Fatal("unable to get database connection string")
			}

			pg, err := db.NewPostgres(connstr)
			if err != nil {
				logger.WithError(err).Fatal("unable to connect to database")
			}

			jwtsecret, err := getJWTSecret()
			if err != nil {
				logger.WithError(err).Fatal("unable to get jwt secret")
			}

			kubernetesAuthInfo, err := getKubernetesAuthInfo()
			if err != nil {
				logger.WithError(err).Fatal("unable to get kubernetes auth info")
			}

			pool := async.NewPool()
			go func() {
				err := pool.Run()

				if err != nil {
					logger.WithError(err).Fatal("unable to start async pool, shutting down")
				}
			}()

			pollch := make(chan types.GitRemote)
			go func() {
				for remote := range pollch {
					logger := logger.WithFields(logrus.Fields{
						"url": remote.URL,
						"ref": remote.Ref,
					})
					logger.Info("creating git poller")

					gp := &gitPoller{
						url:    remote.URL,
						ref:    remote.Ref,
						logger: logger,
						cb: func(url, ref, head string) error {
							logger.WithFields(logrus.Fields{
								"url":  url,
								"ref":  ref,
								"head": head,
							}).Debug("handling git change")

							ctl := ci.Controller{
								URL:            url,
								Ref:            ref,
								Namespace:      kubernetesAuthInfo.namespace,
								ServiceAccount: kubernetesAuthInfo.serviceAccount,
							}

							err := ctl.Bootstrap()
							return err
						},
					}

					pool.AddPoller(fmt.Sprintf("%v#%v", gp.url, gp.ref), gp)
				}
			}()

			logger.WithError(http.NewServer(":9001", pg, jwtsecret, pollch).ListenAndServe()).
				Fatal("got error serving HTTP")
		},
	}
}

func getLogger() *logrus.Entry {
	rawLogLevel := os.Getenv("KPTN_LOG_LEVEL")
	if rawLogLevel == "" {
		rawLogLevel = "info"
	}

	logLevel, err := logrus.ParseLevel(rawLogLevel)
	if err != nil {
		logrus.Warn("unable to parse log level, defaulting to INFO")
		logLevel = logrus.InfoLevel
	}
	logrus.SetLevel(logLevel)

	logger := logrus.WithFields(logrus.Fields{})
	logger.Debugf("setting level to %v", logLevel)

	return logger
}

func getJWTSecret() (string, error) {
	secret := os.Getenv("KPTN_JWT_SECRET")
	if secret == "" {
		return "", errors.New("need KPTN_JWT_SECRET")
	}

	return secret, nil
}

func getDBConnStr() (string, error) {
	pguser := os.Getenv("KPTN_POSTGRES_USER")
	if pguser == "" {
		return "", errors.New("need KPTN_POSTGRES_USER")
	}

	pgpass := os.Getenv("KPTN_POSTGRES_PASS")
	if pgpass == "" {
		return "", errors.New("need KPTN_POSTGRES_PASS")
	}

	pghref := os.Getenv("KPTN_POSTGRES_HREF")
	if pghref == "" {
		return "", errors.New("need KPTN_POSTGRES_HREF")
	}

	pgdb := os.Getenv("KPTN_POSTGRES_DB")
	if pgdb == "" {
		return "", errors.New("need KPTN_POSTGRES_DB")
	}

	pgssl := os.Getenv("KPTN_POSTGRES_SSL")
	if pgssl == "" {
		// Secure by default
		pgssl = "verify-full"
	}

	return fmt.Sprintf("postgres://%v:%v@%v/%v?sslmode=%v",
		pguser, pgpass, pghref, pgdb, pgssl), nil
}

type kubernetesAuth struct {
	namespace      string
	serviceAccount string
}

func getKubernetesAuthInfo() (kubernetesAuth, error) {
	var auth kubernetesAuth

	auth.namespace = os.Getenv("KPTN_KUBERNETES_NAMESPACE")
	if auth.namespace == "" {
		return auth, errors.New("need KPTN_KUBERNETES_NAMESPACE")
	}

	auth.serviceAccount = os.Getenv("KPTN_KUBERNETES_SERVICE_ACCOUNT")
	if auth.serviceAccount == "" {
		return auth, errors.New("need KPTN_KUBERNETES_SERVICE_ACCOUNT")
	}

	return auth, nil
}
