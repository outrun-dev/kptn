package async

import (
	"github.com/sirupsen/logrus"
)

var logger *logrus.Entry

func init() {
	logger = logrus.WithField("package", "async")
}

// SetLogger allows consumers of this package to set a
// package level logger instead of using the default.
func SetLogger(l *logrus.Entry) {
	logger = l
}
