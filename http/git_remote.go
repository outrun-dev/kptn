package http

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/outrun-dev/kptn/types"
	"github.com/sirupsen/logrus"
)

func (srv *Server) handleGitRemotes(rw http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
		handleCreateGitRemote(rw, req, srv.db, srv.pollch)
	}
}

func handleCreateGitRemote(rw http.ResponseWriter, req *http.Request, db store, pollch chan<- types.GitRemote) {
	reqID := req.Context().Value(keyReqID).(string)
	reqSub := req.Context().Value(keyReqSub).(string)
	logger := logger.WithFields(logrus.Fields{
		"request_id":      reqID,
		"request_subject": reqSub,
	})

	logger.Debug("creating git remote")

	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		logger.WithField("error", err).
			Error("unable to read request body")

		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	logger.Debug("unmarshaling request body")
	var gr types.GitRemote
	err = json.Unmarshal(buf, &gr)
	if err != nil {
		logger.WithField("error", err).
			Error("unable to unmarshal request body")

		writeErrResp(rw, err, http.StatusBadRequest)
		return
	}

	logger.Info("saving git remote")
	err = db.CreateGitRemote(reqSub, gr)

	if err != nil {
		logger.WithField("error", err).
			Error("unable to save git repo in database")

		if err == types.ErrGitRemoteExists {
			writeErrResp(rw, err, http.StatusConflict)
			return
		}

		writeErrResp(rw, err, http.StatusInternalServerError)
		return
	}

	// TODO: add a timeout here
	go func() {
		pollch <- gr
	}()

	rw.WriteHeader(http.StatusNoContent)
	return
}
