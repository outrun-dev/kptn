package http

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/sirupsen/logrus"
)

var logger *logrus.Entry

var (
	errNotAuthenticated = errors.New("unable to authenticate user")

	tokenIssuer = "ktpn"
)

func init() {
	logger = logrus.WithField("package", "http")
}

// SetLogger allows consumers of this package to set a
// package level logger instead of using the default.
func SetLogger(l *logrus.Entry) {
	logger = l
}

func writeErrResp(rw http.ResponseWriter, err error, status int) {
	rw.WriteHeader(status)

	buf, err := json.Marshal(map[string]string{
		"error": err.Error(),
	})
	if err != nil {
		return
	}

	rw.Write(buf)
	return
}
