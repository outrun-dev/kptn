package http

import "net/http"

func handleRoot(rw http.ResponseWriter, req *http.Request) {
	rw.WriteHeader(http.StatusNoContent)
	return
}
