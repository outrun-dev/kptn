package kubernetes

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Pod is the virtualized construct that runs on top of
// a node. It's a group of containers with the necessary
// virtualization to provide them the same interface as
// if they were all processes running in the same namespace.
type Pod struct {
	Name string
	// RestartPolicy should be set to one of Kubernetes's supported
	// policies. If zero-value, it will use the Kubernetes default.
	ServiceAccount string
	RestartPolicy  string
	Containers     []Container
	Volumes        []Volume
}

// Volume acts as an attachment from a Pod (a virtualized VM)
// to a PersistentVolumeClaim (a virtualized Volume).
type Volume struct {
	Name                  string
	PersistentVolumeClaim PersistentVolumeClaim
}

// VolumeMount is a mount point on the Pod for the Volume.
type VolumeMount struct {
	// Name identifies the Volume from the Pod's list of Volumes.
	Name string
	Path string
}

// Container is a process running inside a Pod.
type Container struct {
	Name         string
	Image        string
	Command      []string
	Args         []string
	WorkingDir   string
	VolumeMounts []VolumeMount

	// ImagePullPolicy should be set to one of Kubernetes' supported
	// policies. If left blank, it will use the Kubernetes default.
	ImagePullPolicy string
}

// CreatePod creates a Pod in Kubernetes with the passed in definition.
func (k *Client) CreatePod(p Pod) error {
	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: p.Name,
		},
	}

	for _, cnt := range p.Containers {
		kcnt := corev1.Container{
			Name:            cnt.Name,
			Image:           cnt.Image,
			Args:            cnt.Args,
			WorkingDir:      cnt.WorkingDir,
			ImagePullPolicy: corev1.PullPolicy(cnt.ImagePullPolicy),
		}

		if cnt.Command != nil {
			kcnt.Command = cnt.Command
		}

		for _, mnt := range cnt.VolumeMounts {
			kmnt := corev1.VolumeMount{
				Name:      mnt.Name,
				MountPath: mnt.Path,
			}

			kcnt.VolumeMounts = append(kcnt.VolumeMounts, kmnt)
		}

		pod.Spec.Containers = append(pod.Spec.Containers, kcnt)
	}

	for _, vol := range p.Volumes {
		kvol := corev1.Volume{
			Name: vol.Name,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: vol.PersistentVolumeClaim.Name,
				},
			},
		}
		pod.Spec.Volumes = append(pod.Spec.Volumes, kvol)
	}

	if p.RestartPolicy != "" {
		pod.Spec.RestartPolicy = corev1.RestartPolicy(p.RestartPolicy)
	}

	pod.Spec.ServiceAccountName = k.serviceAccount

	_, err := k.clientset.Core().Pods(k.namespace).Create(pod)
	return err
}
