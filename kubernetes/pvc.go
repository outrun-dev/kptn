package kubernetes

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// PersistentVolumeClaim is a virtualized construct on top of a volume,
// much like a Pod is a virtualized construct on top of a node.
type PersistentVolumeClaim struct {
	Name string
	// Size is treated the same way as a size that's requested in the
	// Kubernetes API, so things like 1Gi work.
	Size         string
	StorageClass string
}

// CreatePersistentVolumeClaim creates a ReadWriteMany PVC in Kubernetes
// based on the passed in definition.
func (k *Client) CreatePersistentVolumeClaim(pvc PersistentVolumeClaim) error {
	volsize, err := resource.ParseQuantity(pvc.Size)
	if err != nil {
		return err
	}

	modefs := corev1.PersistentVolumeFilesystem

	kpvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name: pvc.Name,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			VolumeMode: &modefs,
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: volsize,
				},
			},
			StorageClassName: &pvc.StorageClass,
		},
	}

	_, err = k.clientset.Core().PersistentVolumeClaims(k.namespace).Create(kpvc)
	return err
}
