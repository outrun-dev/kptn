package kubernetes

import (
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Client is a simple Kubernetes client with only a
// subset of the features of client-go, as well as
// being namespaced. Clients also act under the authority
// of a single service account.
type Client struct {
	clientset      *kubernetes.Clientset
	namespace      string
	serviceAccount string
}

// NewClient returns a pointer to a Client initialized
// using the configuration bootstrapped with a Pod. For
// this reason it's only meant to be run on Kubernetes.
func NewClient(namespace, serviceAccount string) (*Client, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return &Client{
		clientset:      clientset,
		namespace:      namespace,
		serviceAccount: serviceAccount,
	}, nil
}
