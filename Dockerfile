FROM alpine:3.9

ADD build/kptn /bin/kptn

# This updates the certificate cache to trust certificates from
# very public places like Github.
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

ENTRYPOINT [ "/bin/kptn" ]