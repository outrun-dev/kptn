package ci

import (
	"github.com/google/uuid"
	"gitlab.com/outrun-dev/kptn/kubernetes"
)

const gitImage = "juicemia/git:2.20.1"

// Controller is responsible for bootstrapping and coordinating
// the execution of pipelines for a single change.
type Controller struct {
	URL            string
	Ref            string
	Namespace      string
	ServiceAccount string
}

// Bootstrap is responsible for getting a Git repo's pipelines ready to
// be run.
func (c *Controller) Bootstrap() error {
	logger.Debug("bootstrapping pipelines")

	resourceName := "outrun-bootstrap-" + uuid.New().String()

	client, err := kubernetes.NewClient(c.Namespace, c.ServiceAccount)
	if err != nil {
		return err
	}

	logger.Debug("creating bootstrap pvc")
	pvc := kubernetes.PersistentVolumeClaim{
		Name:         resourceName,
		Size:         "2Gi",
		StorageClass: "standard",
	}
	err = client.CreatePersistentVolumeClaim(pvc)
	if err != nil {
		logger.WithError(err).Debug("unable to create bootstrap pvc")
		return err
	}

	mntname := "repomnt"
	mntdir := "/mnt/repo"
	pod := kubernetes.Pod{
		Name: resourceName,
		Containers: []kubernetes.Container{
			kubernetes.Container{
				Name:  "bootstrap",
				Image: gitImage,
				Args: []string{
					"clone",
					"--single-branch",
					"--branch",
					c.Ref,
					c.URL},
				WorkingDir: mntdir,
				VolumeMounts: []kubernetes.VolumeMount{
					kubernetes.VolumeMount{
						Name: mntname,
						Path: mntdir,
					},
				},
			},
		},
		Volumes: []kubernetes.Volume{
			kubernetes.Volume{
				Name:                  mntname,
				PersistentVolumeClaim: pvc,
			},
		},

		// If this container fails it's safe to assume that CI can't
		// run at this moment so there's no reason to restart. Maybe
		// someday in the future Kubernetes's pod lifecycle features
		// can be leveraged to do intelligent retrying of CI.
		RestartPolicy: "Never",
	}

	logger.Debugf("cloning git using image %v", gitImage)

	logger.Debug("creating bootstrap pod")
	err = client.CreatePod(pod)
	if err != nil {
		logger.WithError(err).Debug("unable to create bootstrap pod")
	}

	return err
}
